import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AddCustomComponent } from './_page/add-custom/add-custom.component';
import { AddYoutubeComponent } from './_page/add-youtube/add-youtube.component';
import { HomePageComponent } from './_page/home-page/home-page.component';
import { VideoPageComponent } from './_page/video-page/video-page.component';

const routes: Routes = [
  {path: "", component: HomePageComponent},
  {path: "add/youtube", component: AddYoutubeComponent},
  {path: "v", component: VideoPageComponent},
  {path: "add/custom", component: AddCustomComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
