import { Component, OnInit } from '@angular/core';
import { AuthenticationResponse } from 'src/app/_model/AuthenticationResponse';
import { AuthService } from 'src/app/_service/auth.service';
import { UIService } from 'src/app/_service/ui.service';

@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.scss']
})
export class LoginPageComponent implements OnInit {

  username: string = "";
  password: string = "";

  showRegister: boolean = false;
  confirmPassword: string = "";
  profilePicture: File | null = null;

  errorMessage: string = "";

  constructor(private authService: AuthService, private uiService: UIService) { }

  ngOnInit(): void {
  }

  submitLogin(): void{
    if (this.username != "" && this.password != ""){
      this.authService.login(this.username, this.password).subscribe((authenticationResponse: AuthenticationResponse) => {
        console.log(authenticationResponse);
        if (authenticationResponse.username != "" && authenticationResponse.authenticationToken != ""){
          this.uiService.changeLoggedIn(true);  
          this.authService.setUsername(authenticationResponse.username);
          this.authService.setJwt(authenticationResponse.authenticationToken);
        }
      })
    }else{
      this.errorMessage = "Please enter a username and password!";
    }
  }

  submitRegister(): void{
    if (this.username != "" && this.password != "" && (this.password === this.confirmPassword) && this.profilePicture != null) {
      this.authService.register(this.username, this.password, this.profilePicture!).subscribe((authenticationResponse: AuthenticationResponse) => {
        console.log(authenticationResponse);
        if (authenticationResponse.username != "" && authenticationResponse.authenticationToken != ""){
          this.uiService.changeLoggedIn(true); 
          this.authService.setUsername(authenticationResponse.username);
          this.authService.setJwt(authenticationResponse.authenticationToken);
        }
      });
    }else{
      this.errorMessage = "Please enter a username, password and picture!";
    }
  }

  bottomButton(): void{
    this.showRegister = !this.showRegister;
  }


  onProfilePictureInput(files: FileList | null): void {
    if (files) {
      this.profilePicture = files.item(0);
    }
  }

}
