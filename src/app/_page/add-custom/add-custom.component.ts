import { HttpEvent, HttpEventType } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Video } from 'src/app/_model/Video';
import { VideoService } from 'src/app/_service/video.service';

import VideoSnapshot from 'video-snapshot';

@Component({
  selector: 'app-add-custom',
  templateUrl: './add-custom.component.html',
  styleUrls: ['./add-custom.component.scss']
})
export class AddCustomComponent implements OnInit {

  title: string = "";
  description: string = "";

  video: File | null = null;
  thumbnail: File | null = null;
  
  doneButton: string = "Done";

  constructor(private videoService: VideoService, private router: Router) { }

  ngOnInit(): void {
  }
  
  onVideoInput(files: FileList | null): void {
    if (files) {
      this.video = files.item(0);
    }
  }

  onThumbnailInput(files: FileList | null): void {
    if (files) {
      this.thumbnail = files.item(0);
    }
  }


  async clickedDone(){

    if (this.title != "" && this.description != "" && this.video != null){

      if (this.thumbnail === null){
          const snapshoter = new VideoSnapshot(this.video!);
          const previewSrc = await snapshoter.takeSnapshot();
          this.thumbnail = this.dataURLtoFile(previewSrc, "thumbnail.png");
      }

      const data: Video = {
        id: "",
        title: this.title,
        description: this.description,
        username: "",
        likes: 0,
        artwork: "",
        isYoutube: false,
        youtubeId: ""
      }

      
      this.videoService.addCustomVideo(data, this.video!, this.thumbnail!)
      .subscribe((event: HttpEvent<any>) => {

        switch(event.type){

          case HttpEventType.Sent:
            console.log("Request has been made!");
            break;

          case HttpEventType.ResponseHeader:
            console.log("Response header has been received!");
            break;

          case HttpEventType.UploadProgress:
            const progress = Math.round(event.loaded / event.total! * 100);
            console.log("Upload: " + progress);
            break;

          case HttpEventType.Response:
            console.log("Success: " + event.body);
            break;

          default:
            console.log("Default Statement in switch");
            break;

        }

      })

      this.router.navigateByUrl("");
      
    }else if (this.video === null){
      alert("Please select a video to upload!");
    }else if (this.title === ""){
      alert("Please enter a title!");
    }else if (this.description === ""){
      alert("Please enter a description!");
    }

  }



  dataURLtoFile(dataurl: string, filename: string) {
 
    var arr = dataurl.split(','),
        mime = arr[0].match(/:(.*?);/)![1],
        bstr = atob(arr[1]), 
        n = bstr.length, 
        u8arr = new Uint8Array(n);
        
    while(n--){
        u8arr[n] = bstr.charCodeAt(n);
    }
    
    return new File([u8arr], filename, {type:mime});

  }

}
