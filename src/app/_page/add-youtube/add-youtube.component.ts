import { typeWithParameters } from '@angular/compiler/src/render3/util';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Video } from 'src/app/_model/Video';
import { FindVideoResponse, Item } from 'src/app/_model/_youtube/FindVideoResponse';
import { VideoService } from 'src/app/_service/video.service';
import { YoutubeService } from 'src/app/_service/youtube.service';

@Component({
  selector: 'app-add-youtube',
  templateUrl: './add-youtube.component.html',
  styleUrls: ['./add-youtube.component.scss']
})
export class AddYoutubeComponent implements OnInit {

  youtubeLink: string = "";
  private timeRemaining = 1;

  title: string = "";
  description: string = "";
  imageUrl: string = "";

  doneButton: string = "Done";

  private foundVideo?: Item;

  constructor(private youtubeService: YoutubeService, private videoService: VideoService, private router: Router) { }

  ngOnInit(): void {
    this.countDown();
  }


  checkVideo(): void{

    if (this.youtubeLink.includes("watch?v=") && this.youtubeLink.length > 20){

      const index = this.youtubeLink.indexOf("watch?v=");
      const videoId = this.youtubeLink.substring(index+8, (index+8)+11);
      
      this.youtubeService.findVideoById(videoId).subscribe((response: FindVideoResponse) => {
        if (response.items.length > 0){
          this.foundVideo = response.items[0];
          this.title = response.items[0].snippet.title;
          this.description = response.items[0].snippet.description;

          if (response.items[0].snippet.thumbnails.maxres != undefined){
            this.imageUrl = response.items[0].snippet.thumbnails.maxres.url;
          } else if (response.items[0].snippet.thumbnails.high != undefined){
            this.imageUrl = response.items[0].snippet.thumbnails.high.url;
          }

        }else{
          this.foundVideo = undefined;
          this.title = "";
          this.description = "";
        }
      })

    }

  }

  changeOfInputField(): void{
    this.timeRemaining = 1;
    console.log(this.youtubeLink);
  }

  countDown(): void{
    setTimeout(() => {
      if (this.timeRemaining > 0){
        this.timeRemaining --;
        if (this.timeRemaining === 0 && this.youtubeLink != ""){
          console.log("check video");
          this.checkVideo();
        }
      }
      this.countDown();
    }, 1000)
  }


  clickedDone(): void{
    this.doneButton = "Loading..."
    if (this.foundVideo != null){
      const video = {
        id: "",
        title: this.foundVideo.snippet.title,
        description: this.foundVideo.snippet.description,
        username: this.foundVideo.snippet.channelTitle,
        likes: 0,
        artwork: `https://img.youtube.com/vi/${this.foundVideo.id}/{s}.jpg`,
        isYoutube: true, 
        youtubeId: this.foundVideo.id
      }
      this.videoService.addYoutubeVideo(video).subscribe((video: Video) => {
        this.router.navigateByUrl("");
      });
    }

  }

}
