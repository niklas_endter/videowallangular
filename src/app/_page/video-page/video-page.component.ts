import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { Video } from 'src/app/_model/Video';
import { VideoService } from 'src/app/_service/video.service';

import videojs from 'video.js';
import 'video.js/dist/video-js.css';


@Component({
  selector: 'app-video-page',
  templateUrl: './video-page.component.html',
  styleUrls: ['./video-page.component.scss']
})
export class VideoPageComponent implements OnInit {

  videoId: string = "";
  video?: Video;
  imageUrl: string = "";
  
  iframeUrl: string = "";
  safe: string = "";

  player: any;
  private tries: number = 0;

  constructor(private route: ActivatedRoute, private videoService: VideoService) {
    this.route
    .queryParams.subscribe((params: Params) => {
      
      this.videoId = params["id"];
      
      if (this.videoId != ""){
        this.videoService.findVideo(this.videoId).subscribe((video: Video) => {
          this.video = video;

          if (this.video.isYoutube){
            this.iframeUrl = "https://www.youtube.com/embed/" + this.video.youtubeId;
            this.imageUrl = this.video.artwork.replace("{s}", "maxresdefault")
          }else{

            this.imageUrl = this.video.artwork.replace("{s}", "900")

          }

        })
      }

    })
  }

  ngOnInit(): void {
  }

  ngAfterViewInit(): void{

    this.setupVideoJS();

  }

  ngOnDestroy(): void {
    this.videoId = "";
    if (this.player) {
      this.player.dispose();
    }
  }

  setupVideoJS(){

    this.tries++;
    console.log("setupVideoJS: " + document.getElementById('video-player'));

    const videoElement = document.getElementById('video-player');
    if (videoElement != null){
      this.player = new videojs(document.getElementById('video-player'), 
        {
          sources: {
            src: "http://localhost:8080/m/vids/" + this.videoId + "/main.m3u8",
            type: "application/x-mpegURL"
          },
          fluid: true,
          live: false
        },
        function onPlayerReady() {
          console.log("Player ready");
        },
      )
      console.log("initialized player");
    }else{
      setTimeout(() => {
        if (this.tries < 50){
          this.setupVideoJS()
        }
      }, 50);
    }

  }

}
