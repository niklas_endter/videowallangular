import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/_service/auth.service';
import { UIService } from 'src/app/_service/ui.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {

  username: String = "";

  constructor(private authService: AuthService, private uiService: UIService) {
    this.username = authService.getUsername();
  }

  ngOnInit(): void {
  }
  
  showMenu(): void {
    this.uiService.changeMenu(true);
  }

}
