import { Location } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/_service/auth.service';
import { UIService } from 'src/app/_service/ui.service';

@Component({
  selector: 'app-navbar-back',
  templateUrl: './navbar-back.component.html',
  styleUrls: ['./navbar-back.component.scss']
})
export class NavbarBackComponent implements OnInit {

  username: String = "";

  constructor(private authService: AuthService, private uiService: UIService, private location: Location) {
    this.username = authService.getUsername();
  }

  ngOnInit(): void {
  }

  goBack(): void {
    this.location.back();
  }

  showMenu(): void {
    this.uiService.changeMenu(true);
  }

}
