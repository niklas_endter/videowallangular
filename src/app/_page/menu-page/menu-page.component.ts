import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/_service/auth.service';
import { UIService } from 'src/app/_service/ui.service';

@Component({
  selector: 'app-menu-page',
  templateUrl: './menu-page.component.html',
  styleUrls: ['./menu-page.component.scss']
})
export class MenuPageComponent implements OnInit {

  constructor(private uiService: UIService, private router: Router, private authService: AuthService) { }

  ngOnInit(): void {
  }

  clickedClose(): void{
    this.uiService.changeMenu(false);
  }

  clickedAddYoutube(): void{
    this.uiService.changeMenu(false);
    this.router.navigateByUrl("add/youtube");
  }

  clickedLogout(): void{
    this.uiService.changeMenu(false);
    this.authService.logout();
  }

  clickedAddCustom(): void{
    this.uiService.changeMenu(false);
    this.router.navigateByUrl("add/custom");
  }

}
