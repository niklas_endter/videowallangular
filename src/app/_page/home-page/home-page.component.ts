import { Component, OnInit } from '@angular/core';
import { Collection } from 'src/app/_model/Collection';
import { Video } from 'src/app/_model/Video';
import { VideoService } from 'src/app/_service/video.service';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.scss']
})
export class HomePageComponent implements OnInit {

  collections: Collection[] = [];

  types = ["none", "one", "two", "threeright", "threeleft"];

  constructor(private videoService: VideoService) { }

  ngOnInit(): void {
    this.getVideos(0);
  }

  getVideos(offset: number): void{
    this.videoService.getVideos(offset).subscribe((videos: Video[]) => {
      console.log(JSON.stringify(videos));
      this.generateCollections(videos);
    })
  }

  private generateCollections(videos: Video[]){

    let continueLoop = true;
    let usedVideos: number = 0;
    let newCollections: Collection[] = [];

    while (continueLoop){

      let amount = Math.floor((Math.random()*4+1));

      if (amount > videos.length-usedVideos){
        console.log("amount to big: " + amount + ", videos: " + (videos.length-usedVideos));
        amount = videos.length-usedVideos;
      }

      let collection: Collection;

      console.log("amount: " + amount);

      if (amount === 4){
        collection = {
          type: this.types[amount],
          videos: videos.slice(usedVideos, usedVideos+3)
        }
      }else{
        collection = {
          type: this.types[amount],
          videos: videos.slice(usedVideos, usedVideos+amount)
        }
      }

      if (collection === null || collection.videos.length === 0){
        break;
      }

      newCollections.push(collection);
      
      usedVideos += amount;
      if (usedVideos >= videos.length){
        
        for (collection of newCollections){
          console.log("final: " + collection.type, collection.videos.length)
        }

        continueLoop = false;
      }

    }

    this.collections = this.collections.concat(newCollections);

  }

}