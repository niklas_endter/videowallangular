import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MediumVideoComponent } from './medium-video.component';

describe('MediumVideoComponent', () => {
  let component: MediumVideoComponent;
  let fixture: ComponentFixture<MediumVideoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MediumVideoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MediumVideoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
