import { Component, Input, OnInit } from '@angular/core';

import {
  trigger,
  state,
  style,
  animate,
  transition,
} from '@angular/animations';
import { Video } from 'src/app/_model/Video';
import { Router } from '@angular/router';

@Component({
  selector: 'app-small-video',
  templateUrl: './small-video.component.html',
  styleUrls: ['./small-video.component.scss']
})
export class SmallVideoComponent implements OnInit {

  @Input() video!: Video;
  imageUrl: string = "";

  constructor(private router: Router) { }

  ngOnInit(): void {
    if (this.video.isYoutube){
      this.imageUrl = this.video.artwork.replace("{s}", "maxresdefault")
    }else{
      this.imageUrl = this.video.artwork.replace("{s}", "900")
    }
  }

  clicked(): void{
    this.router.navigateByUrl("/v?id=" + this.video.id)
  }

}
