import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Video } from 'src/app/_model/Video';

@Component({
  selector: 'app-large-video',
  templateUrl: './large-video.component.html',
  styleUrls: ['./large-video.component.scss']
})
export class LargeVideoComponent implements OnInit {

  @Input() video!: Video;
  imageUrl: string = "";

  constructor(private router: Router) { }

  ngOnInit(): void {
    if (this.video.isYoutube){
      this.imageUrl = this.video.artwork.replace("{s}", "maxresdefault")
    }else{
      this.imageUrl = this.video.artwork.replace("{s}", "900")
    }
  }

  clicked(): void{
    this.router.navigateByUrl("/v?id=" + this.video.id)
  }

}
