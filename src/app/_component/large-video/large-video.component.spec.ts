import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LargeVideoComponent } from './large-video.component';

describe('LargeVideoComponent', () => {
  let component: LargeVideoComponent;
  let fixture: ComponentFixture<LargeVideoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LargeVideoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LargeVideoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
