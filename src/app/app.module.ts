import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgxFileDropModule } from 'ngx-file-drop';

import { AppComponent } from './app.component';
import { LoginPageComponent } from './_page/login-page/login-page.component';
import { HomePageComponent } from './_page/home-page/home-page.component';
import { NavbarComponent } from './_page/navbar/navbar.component';
import { SmallVideoComponent } from './_component/small-video/small-video.component';
import { MediumVideoComponent } from './_component/medium-video/medium-video.component';
import { LargeVideoComponent } from './_component/large-video/large-video.component';
import { MenuPageComponent } from './_page/menu-page/menu-page.component';
import { AddYoutubeComponent } from './_page/add-youtube/add-youtube.component';
import { NavbarBackComponent } from './_page/navbar-back/navbar-back.component';
import { VideoPageComponent } from './_page/video-page/video-page.component';
import { SafePipe } from './_extra/SafePipe';
import { AddCustomComponent } from './_page/add-custom/add-custom.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginPageComponent,
    HomePageComponent,
    NavbarComponent,
    SmallVideoComponent,
    MediumVideoComponent,
    LargeVideoComponent,
    MenuPageComponent,
    AddYoutubeComponent,
    NavbarBackComponent,
    VideoPageComponent,

    SafePipe,
     AddCustomComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    BrowserAnimationsModule,
    NgxFileDropModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
