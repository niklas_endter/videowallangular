import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { FindVideoResponse } from '../_model/_youtube/FindVideoResponse';
import { Observable } from 'rxjs';
import { GlobalConstants } from '../_util/Constants';


@Injectable({
  providedIn: 'root'
})
export class YoutubeService {

  constructor(private http: HttpClient) { }

  findVideoById(videoId: string): Observable<FindVideoResponse>{
    return this.http.get<FindVideoResponse>(GlobalConstants.youtubeUrl + "videos?id=" + videoId + "&key=" + GlobalConstants.youtubeApiKey + "&part=snippet,contentDetails,statistics,status");
  }

}
