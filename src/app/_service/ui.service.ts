import { Injectable } from '@angular/core';
import { Subject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UIService {

  private loginSubject = new Subject<any>();
  private menuSubject = new Subject<any>();
  
  constructor() { }

  changeLoggedIn(loggedIn: boolean): void{
    this.loginSubject.next(loggedIn);
  }
  onChangeLoggedIn(): Observable<any>{
    return this.loginSubject.asObservable();
  }


  changeMenu(menu: boolean): void{
    this.menuSubject.next(menu);
  }
  onChangeMenu(): Observable<any>{
    return this.menuSubject.asObservable();
  }

}
