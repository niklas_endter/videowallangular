import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Video } from '../_model/Video';
import { Observable } from 'rxjs';
import { GlobalConstants } from '../_util/Constants';
import { AuthService } from './auth.service';
import { YoutubeService } from './youtube.service';
import { FindVideoResponse } from '../_model/_youtube/FindVideoResponse';


@Injectable({
  providedIn: 'root'
})
export class VideoService {

  constructor(private http: HttpClient, private authService: AuthService, private youtubeService: YoutubeService) {
  }

  

  getVideos(offset: number): Observable<Video[]>{
    const httpOptions = {
      headers: new HttpHeaders({
        'Authorization': 'Bearer ' + this.authService.getJwt(),
        'Content-Type': 'application/json',
      })
    }
    return this.http.get<Video[]>(GlobalConstants.apiUrl + "v/start?offset=" + offset, httpOptions);
  }


  addYoutubeVideo(video: Video): Observable<Video>{
    const reqHeader = new HttpHeaders({ 
      'Authorization': 'Bearer ' + this.authService.getJwt(),
      'Content-Type': 'application/json'
    });
    return this.http.post<Video>(GlobalConstants.apiUrl + "v/add/youtube", video, {headers: reqHeader});
  }



  addCustomVideo(data: Video,  video: File, thumbnail: File): Observable<any>{
    
    const reqHeader = new HttpHeaders({ 
      'Authorization': 'Bearer ' + this.authService.getJwt()
    });

    const formData = new FormData();
    formData.append("json", JSON.stringify(data));
    formData.append("video", video);
    formData.append("thumbnail", thumbnail);

    return this.http.post<Video>(GlobalConstants.apiUrl + "v/add/custom", formData, {headers: reqHeader, reportProgress: true, observe: 'events'});

  }





  findVideo(id: string): Observable<Video>{
    const reqHeader = new HttpHeaders({ 
      'Authorization': 'Bearer ' + this.authService.getJwt(),
      'Content-Type': 'application/json'
    });
    return this.http.get<Video>(GlobalConstants.apiUrl + "v/find?id=" + id, {headers: reqHeader});
  }


  addYoutubeVideoWithUrl(url: string){

    if (url.includes("watch?v=") && url.length > 20){

      const index = url.indexOf("watch?v=");
      const videoId = url.substring(index+8, (index+8)+11);

      this.youtubeService.findVideoById(videoId).subscribe((response: FindVideoResponse) => {

        if (response.items.length > 0){

          const foundVideo = response.items[0];
          if (foundVideo != null){
            const video = {
              id: "",
              title: foundVideo.snippet.title,
              description: foundVideo.snippet.description,
              username: foundVideo.snippet.channelTitle,
              likes: 0,
              artwork: `https://img.youtube.com/vi/${foundVideo.id}/{s}.jpg`,
              isYoutube: true, 
              youtubeId: foundVideo.id
            }
            this.addYoutubeVideo(video).subscribe((video: Video) => {
              console.log("Added video!")
            });
          }

        }

      })

    }

  }

}
