import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AuthenticationResponse } from '../_model/AuthenticationResponse';
import { Observable } from 'rxjs';
import { GlobalConstants } from '../_util/Constants';
import { UIService } from './ui.service';


const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json',
  })
}


@Injectable({
  providedIn: 'root'
})
export class AuthService {

  public isLoggedIn: boolean = false;

  private username: string = "";
  private jwt: string = "";

  constructor(private http: HttpClient, private uiService: UIService) {
    this.getCredentials();
  }


  // MAIN
  getCredentials(): void{
    this.username = localStorage.getItem("username") || "";
    this.jwt = localStorage.getItem("jwt") || "";

    
    if (this.username != "" && this.jwt != ""){
      console.log("getCredentials: " + this.username + ", " + this.jwt);
      this.isLoggedIn = true;
      this.uiService.changeLoggedIn(true);
    }

  }


  // API
  login(username: string, password: string): Observable<AuthenticationResponse>{
    const data = {"username": username, "password": password};
    const json = JSON.stringify(data);
    return this.http.post<AuthenticationResponse>(GlobalConstants.apiUrl + "auth/login", json, httpOptions);
  }

  register(username: string, password: string, file: File): Observable<AuthenticationResponse>{
    const data = {"username": username, "password": password};
    const json = JSON.stringify(data);

    const formData = new FormData();
    formData.append("json", json);
    formData.append("file", file);

    return this.http.post<AuthenticationResponse>(GlobalConstants.apiUrl + "auth/signup", formData);
  }

  logout(): void{
    localStorage.removeItem("username");
    localStorage.removeItem("jwt");
    this.uiService.changeLoggedIn(false);
  }


  setUsername(username: string): void{
    this.username = username;
    localStorage.setItem("username", this.username);
  }
  setJwt(jwt: string): void{
    this.jwt = jwt;
    localStorage.setItem("jwt", this.jwt);
  }
  getUsername(): string{
    return this.username;
  }
  getJwt(): string{
    return this.jwt;
  }

}
