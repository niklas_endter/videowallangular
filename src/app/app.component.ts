import { Location } from '@angular/common';
import { Component } from '@angular/core';
import { AuthService } from './_service/auth.service';
import { UIService } from './_service/ui.service';

import { NgxFileDropEntry, FileSystemFileEntry, FileSystemDirectoryEntry } from 'ngx-file-drop';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  loggedIn: boolean = false;
  menu: boolean = false;
  
  constructor(private authService: AuthService, private uiService: UIService){
    
    this.loggedIn = authService.isLoggedIn;
    this.uiService.onChangeLoggedIn().subscribe((value: boolean) => {
      this.loggedIn = value;

      if (this.loggedIn){
        window.location.reload();
      }

    })

    this.uiService.onChangeMenu().subscribe((value: boolean) => {
      this.menu = value;
    })

  }

  ngOnInit(): void {
  }


  //DROPING
  public files: NgxFileDropEntry[] = [];

  public dropped(files: NgxFileDropEntry[]) {
    
    this.files = files;

    for (const droppedFile of files) {

      if (droppedFile.fileEntry.isFile) {
        const fileEntry = droppedFile.fileEntry as FileSystemFileEntry;

        fileEntry.file((file: File) => {
          
          // 

        });

      }

    }
  }

  public fileOver(event: any){
    console.log(event);
  }

  public fileLeave(event: any){
    console.log(event);
  }

}
