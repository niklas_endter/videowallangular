import { Video } from './Video';

export interface Collection {
    type: string,
    videos: Video[],
}