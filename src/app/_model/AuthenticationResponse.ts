export interface AuthenticationResponse{
    authenticationToken: string,
    username: string
}