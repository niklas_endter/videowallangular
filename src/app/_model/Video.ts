export interface Video{
    id: string,
    title: string,
    description: string,
    username: string,
    likes: number,
    artwork: string,
    isYoutube: boolean,
    youtubeId: string
}