// To parse this data:
//
//   import { Convert, Welcome } from "./file";
//
//   const welcome = Convert.toWelcome(json);
//
// These functions will throw an error if the JSON doesn't
// match the expected interface, even if the JSON is valid.

export interface FindVideoResponse {
    kind:     string;
    etag:     string;
    items:    Item[];
    pageInfo: PageInfo;
}

export interface Item {
    kind:           string;
    etag:           string;
    id:             string;
    snippet:        Snippet;
    contentDetails: ContentDetails;
    status:         Status;
    statistics:     Statistics;
}

export interface ContentDetails {
    duration:        string;
    dimension:       string;
    definition:      string;
    caption:         string;
    licensedContent: boolean;
    contentRating:   ContentRating;
    projection:      string;
}

export interface ContentRating {
}

export interface Snippet {
    publishedAt:          Date;
    channelId:            string;
    title:                string;
    description:          string;
    thumbnails:           Thumbnails;
    channelTitle:         string;
    tags:                 string[];
    categoryId:           string;
    liveBroadcastContent: string;
    localized:            Localized;
    defaultAudioLanguage: string;
}

export interface Localized {
    title:       string;
    description: string;
}

export interface Thumbnails {
    default:  Default;
    medium:   Default;
    high:     Default;
    standard: Default;
    maxres:   Default;
}

export interface Default {
    url:    string;
    width:  number;
    height: number;
}

export interface Statistics {
    viewCount:     string;
    likeCount:     string;
    favoriteCount: string;
    commentCount:  string;
}

export interface Status {
    uploadStatus:        string;
    privacyStatus:       string;
    license:             string;
    embeddable:          boolean;
    publicStatsViewable: boolean;
    madeForKids:         boolean;
}

export interface PageInfo {
    totalResults:   number;
    resultsPerPage: number;
}